var express = require('express'),
  app = express(),
  port = process.env.PORT || 3030;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })

  app.listen(port);

  var requestJson = require('request-json');

  var path = require('path');


  app.get('/cliente/:idCliente', function(req, res){
      var result = {
        nombre:"John",
        apellidos:"Doe",
        saldo_mxn:400000.00,
        saldo_btc:3,
        usr_name:"johnDoe",
        usr_pwd:"password",
        porcentaje_inversion:80

      };

      var jsonResult = JSON.stringify(result);
      res.send(jsonResult);

    });

  app.get('/getCurrentPrice/:currencyType', function(req, res){

      var result = {
        currencyType:"btc",
        currencyPrice:220000.00

      };

      var jsonResult = JSON.stringify(result);
      res.send(jsonResult);

    })

  app.get('/login/:idCliente/:password', function(req, res){

    })

  app.get('/register/:clientInfo', function(req, res){

    })

  app.post('/sell/:amount', function(req, res){
    if (req.params.amount > 3){
      res.status(500).send("saldo insuficiente")
    }else{
      res.status(200).send("monedas vendidas")
    }
    })

  app.post('/buy/:amount', function(req, res){
    if (req.params.amount > 3){
      res.status(500).send("saldo insuficiente")
    }else{
      res.status(200).send("monedas compradas")
    }

  })
